<?php

/**
 * Zeraga_Newsletter
 *
 * @category    Zeraga
 * @package     Zeraga_Newsletter
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Zeraga_Newsletter_Model_Bestseller extends Mage_Core_Model_Abstract
{
    const BESTSELLER_ENABLE = 'zeraga_newsletter/bestseller/enable';
    const BESTSELLER_PERIOD = 'zeraga_newsletter/bestseller/period';
    const BESTSELLER_HEADING = 'zeraga_newsletter/bestseller/heading';
    const BESTSELLER_CATEGORY = 'zeraga_newsletter/bestseller/category';
    const BESTSELLER_AMOUNT = 'zeraga_newsletter/bestseller/amount'; // amount of product will be showed up in email

    protected $_storeId;


    public function setStoreId($storeid)
    {
        $this->_storeId = $storeid;
        return $this;
    }

    public function getStoreId()
    {
        return $this->_storeId;
    }

    /**
     * Get if featured product display is enabled or not
     *
     * @return boolean
     */

    public function isEnable()
    {
        $storeId = $this->getStoreId();
        return Mage::getStoreConfig(self::BESTSELLER_ENABLE, $storeId);
    }

    /**
     * Get bestseller in specific store
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function getBestsellerCollection()
    {
        $storeId = $this->getStoreId();
        /**@var $zendDb Zend_Db_Adapter_Abstract */
        $zendDb = Mage::helper('zeraga_newsletter/sql')->getZendDB();
        $resource = Mage::getSingleton('core/resource');

        $select = $zendDb->select()
            ->from(
                array('items' => $resource->getTableName('sales/order_item')), // table name
                array('date_create_at' => 'date(created_at)', 'qty_ordered' => 'sum(qty_ordered)', 'product_id') //  selected columns
            );
        $select = $this->_applyPeriodCondition($select);

        $productIds = $this->_getProductIds(); // Only get product from some preset category
        if (!empty($productIds)) {
            $select->where('product_id in (?)', $productIds);
        }
        // Website filter
        if (!empty($storeId)) {
            $select->where('store_id = ?', $storeId);
        }
        $select->group(array('product_id'))
            ->order('qty_ordered ASC');
        $stmt = $zendDb->query($select);
        $productIds = $stmt->fetchAll(PDO::FETCH_COLUMN, 2);

        $productCollection = Mage::getModel('catalog/product')->getCollection();
        $productCollection->addAttributeToSelect('*');
        if (!empty($productIds)) {
            $productCollection->addAttributeToFilter('entity_id', array('IN' => $productIds));
        }
        $amount = Mage::getStoreConfig(self::BESTSELLER_AMOUNT, $storeId);
        $productCollection->getSelect()->limit($amount, 0);
        return $productCollection;
    }

    /**
     * Get all product ids in preset categories
     * @return array
     */
    protected function _getProductIds()
    {
        $storeId = $this->getStoreId();
        $categoryList = Mage::getStoreConfig(self::BESTSELLER_CATEGORY, $storeId);
        if ($found = strpos($categoryList, ',')  !== false) {
            $categoryList = explode(',',$categoryList);
        } else {
            $categoryList = array($categoryList);
        }

        $categoryResult = array();
        foreach($categoryList as $catId) {
            $category = Mage::getModel('catalog/category')->load($catId);
            if($category->getId()) {
                $categoryResult = array_merge($categoryResult, $category->getAllChildren(true));
            }
        }
        $categoryResult = array_unique($categoryResult);

        /**@var $zendDb Zend_Db_Adapter_Abstract */
        $zendDb = Mage::helper('zeraga_newsletter/sql')->getZendDB();
        $resource = Mage::getSingleton('core/resource');
        if (!empty($categoryResult)) {
            $select = $zendDb->select()
                ->from(array('cat' => $resource->getTableName('catalog/category_product')))
                ->where('category_id IN (?)', $categoryResult);
        } else {
            $select = $zendDb->select()
                ->from(array('cat' => $resource->getTableName('catalog/category_product')));
        }

        $stmt = $zendDb->query($select);
        $productIds = array_unique($stmt->fetchAll(PDO::FETCH_COLUMN, 1));

        return $productIds;
    }

    /**
     * @param $select Zend_Db_Select
     * @return Zend_Db_Select
     */
    protected function _applyPeriodCondition($select)
    {
        $storeId = $this->getStoreId();
        $period = Mage::getStoreConfig(self::BESTSELLER_PERIOD, $storeId);
        switch ($period) {
            case 1:
                $select->where('date(created_at) > DATE_SUB(curdate(),INTERVAL 30 DAY)');
                break;
            case 2:
                $select->where('date(created_at) > DATE_SUB(curdate(),INTERVAL 60 DAY)');
                break;
            case 3:
                $select->where('date(created_at) > DATE_SUB(curdate(),INTERVAL 1 YEAR)');
                break;
            case 4:
                $today = date('Y-m-d');
                $todayArr = explode('-', $today);
                $dayFrom = ($todayArr['0'] - 1) . '-' . $todayArr['1'] . '-01';
                $dayTo = date('Y-m-t', strtotime($dayFrom));
                $select->where('date(created_at) > DATE(' . $dayFrom . ') AND date(created_at) < DATE(' . $dayTo . ')');
                break;
            case 5:
                $select->where('date(created_at) < DATE(curdate())');
                break;
            default:
                $select->where('date(created_at) < DATE(curdate())');
                break;
        }
        return $select;
    }

    /**
     * Apply amount limit to query expression
     * @return Zend_Db_Select
     */
    protected function _applyAmount($select)
    {
        $storeId = $this->getStoreId();
        $amount = Mage::getStoreConfig(self::BESTSELLER_AMOUNT, $storeId);
        $select->limit($amount, 0);
        return $select;
    }

}