<?php
/**
 * Zeraga_Newsletter
 *
 * @category    Zeraga
 * @package     Zeraga_Newsletter
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Zeraga_Newsletter_Model_Newproduct extends Mage_Core_Model_Abstract
{
    const NEWPRODUCT_ENABLE = 'zeraga_newsletter/newproduct/enable';
    const NEWPRODUCT_HEADING = 'zeraga_newsletter/newproduct/heading';
    const NEWPRODUCT_CATEGORY = 'zeraga_newsletter/newproduct/category';
    const NEWPRODUCT_AMOUNT = 'zeraga_newsletter/newproduct/amount'; // amount of product will be showed up in email

    protected $_storeId;


    public function setStoreId($storeid)
    {
        $this->_storeId = $storeid;
        return $this;
    }

    public function getStoreId()
    {
        return $this->_storeId;
    }

    /**
     * Get if new product block display is enabled or not
     *
     * @return boolean
     */
    public function isEnable()
    {
        $storeId = $this->getStoreId();
        return Mage::getStoreConfig(self::NEWPRODUCT_ENABLE, $storeId);
    }

    /**
     * get new product collection base on configuration
     *
     * @return Mage_Catalog_Model_Resource_Product_Collection
    */
    public function getNewproductCollection() {
        $storeId = $this->getStoreId();

        $productIds = $this->_getProductIds();
        $productCollection = Mage::getModel('catalog/product')->getCollection();
        $productCollection->addAttributeToSelect('*');
        $productCollection->addAttributeToFilter('status', array('eq' => 1));
        $productCollection->addAttributeToFilter('visibility', array('neq' => 1));
        if (!empty($productIds)) {
            $productCollection->addAttributeToFilter('entity_id', array('in', $productIds));
        }
        $productCollection->getSelect()->order('entity_id DESC');
        // limit product
        $amount = Mage::getStoreConfig(self::NEWPRODUCT_AMOUNT, $storeId);
        $productCollection->getSelect()->limit($amount, 0);
        return $productCollection;

    }

    /**
     * Get all product ids in preset categories
     * @return array
    */
    protected function _getProductIds()
    {
        $storeId = $this->getStoreId();
        $categoryList = Mage::getStoreConfig(self::NEWPRODUCT_CATEGORY, $storeId);
        if ($found = strpos($categoryList, ',')  !== false) {
            $categoryList = explode(',',$categoryList);
        } else {
            $categoryList = array($categoryList);
        }

        $categoryResult = array();
        foreach($categoryList as $catId) {
            $category = Mage::getModel('catalog/category')->load($catId);
            $categoryResult = array_merge($categoryResult, $category->getAllChildren(true));
        }
        $categoryResult = array_unique($categoryResult);

        /**@var $zendDb Zend_Db_Adapter_Abstract */
        $zendDb = Mage::helper('zeraga_newsletter/sql')->getZendDB();
        $resource = Mage::getSingleton('core/resource');

        $select = $zendDb->select()
            ->from(array('cat' => $resource->getTableName('catalog/category_product')))
            ->where('category_id IN (?)', $categoryResult);
        $stmt = $zendDb->query($select);
        $productIds = array_unique($stmt->fetchAll(PDO::FETCH_COLUMN, 1));

        return $productIds;
    }

}

