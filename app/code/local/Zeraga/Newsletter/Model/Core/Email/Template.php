<?php

/**
 * Zeraga_Newsletter
 *
 * @category    Zeraga
 * @package     Zeraga_Newsletter
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Zeraga_Newsletter_Model_Core_Email_Template extends Mage_Core_Model_Email_Template
{
    /**
     * Change the way magento sending email
     *
     * @param $email string : email of receiver
     * @param $name string : name of receiver
     * @param $variables array : config of email setting
     *
     */
    public function send($email, $name = null, array $variables = array(), $transport = false)
    {
        if (Mage::helper('zeraga_newsletter/mail')->sendMailUsingSmtp() != true || !$transport) {
            return parent::send($email, $name, $variables);
        }

        if (!$this->isValidForSend()) {
            Mage::log('SMTP: Email not valid for sending - check template, and smtp enabled/disabled setting');
            Mage::logException(new Exception('This letter cannot be sent.')); // translation is intentionally omitted
            return false;
        }

        $emails = array_values((array)$email);
        $names = is_array($name) ? $name : (array)$name;
        $names = array_values($names);
        foreach ($emails as $key => $email) {
            if (!isset($names[$key])) {
                $names[$key] = substr($email, 0, strpos($email, '@'));
            }
        }

        $variables['email'] = reset($emails);
        $variables['name'] = reset($names);

        $mail = $this->getMail();

        // In Magento core they set the Return-Path here, for the sendmail command.
        // we assume our outbound SMTP server (or Gmail) will set that.

        foreach ($emails as $key => $email) {
            $mail->addTo($email, '=?utf-8?B?' . base64_encode($names[$key]) . '?=');
        }

        $this->setUseAbsoluteLinks(true);
        $text = $this->getProcessedTemplate($variables, true);

        if ($this->isPlain()) {
            $mail->setBodyText($text);
        } else {
            $mail->setBodyHTML($text);
        }

        $mail->setSubject('=?utf-8?B?' . base64_encode($this->getProcessedTemplateSubject($variables)) . '?=');
        $mail->setFrom($this->getSenderEmail(), $this->getSenderName());
        if (!$transport) {
            $transporter = Mage::helper('zeraga_newsletter/mail')->getTransport($this->getDesignConfig()->getStore());
        } else {
            $transporter = $transport;
        }

        try {
            Mage::log('About to send email');
            $mail->send($transporter); // Zend_Mail warning..
            Mage::log('Finished sending email');

            $this->_mail = null;
        } catch (Exception $e) {
            Mage::logException($e);
            return false;
        }
        return true;
    }

    /**
     * Send transactional email to recipient
     *
     * @param   int $templateId
     * @param   string|array $sender sneder informatio, can be declared as part of config path
     * @param   string $email recipient email
     * @param   string $name recipient name
     * @param   array $vars varianles which can be used in template
     * @param   false|Zend_Mail_Transport_Smtp $transport
     * @param   int|null $storeId
     * @return  Mage_Core_Model_Email_Template
     */
    public function sendTransactional($templateId, $sender, $email, $name, $vars=array(), $storeId=null, $transport = false)
    {
        $this->setSentSuccess(false);
        if (($storeId === null) && $this->getDesignConfig()->getStore()) {
            $storeId = $this->getDesignConfig()->getStore();
        }

        if (is_numeric($templateId)) {
            $this->load($templateId);
        } else {
            $localeCode = Mage::getStoreConfig('general/locale/code', $storeId);
            $this->loadDefault($templateId, $localeCode);
        }

        if (!$this->getId()) {
            throw Mage::exception('Mage_Core', Mage::helper('core')->__('Invalid transactional email code: %s', $templateId));
        }

        if (!is_array($sender)) {
            $this->setSenderName(Mage::getStoreConfig('trans_email/ident_' . $sender . '/name', $storeId));
            $this->setSenderEmail(Mage::getStoreConfig('trans_email/ident_' . $sender . '/email', $storeId));
        } else {
            $this->setSenderName($sender['name']);
            $this->setSenderEmail($sender['email']);
        }

        if (!isset($vars['store'])) {
            $vars['store'] = Mage::app()->getStore($storeId);
        }
        $this->setSentSuccess($this->send($email, $name, $vars, $transport));
        return $this;
    }

    /**
     * Process email template code
     *
     * @param   array $variables
     * @return  string
     */
    public function getProcessedTemplate(array $variables = array())
    {
        $processor = $this->getTemplateFilter();
        $processor->setUseSessionInUrl(false)
            ->setPlainTemplateMode($this->isPlain());

        if (!$this->_preprocessFlag) {
            $variables['this'] = $this;
        }

        if (isset($variables['subscriber']) && ($variables['subscriber'] instanceof Mage_Newsletter_Model_Subscriber)) {
            $processor->setStoreId($variables['subscriber']->getStoreId());
        }
        if (method_exists($this, '_getLogoUrl')) {
            if (!isset($variables['logo_url'])) {
                $variables['logo_url'] = $this->_getLogoUrl($processor->getStoreId());
            }
        }
        if (method_exists($this, '_getLogoAlt')) {
            if (!isset($variables['logo_alt'])) {
                $variables['logo_alt'] = $this->_getLogoAlt($processor->getStoreId());
            }
        }

        $processor->setIncludeProcessor(array($this, 'getInclude'))
            ->setVariables($variables);

        $this->_applyDesignConfig();
        try {
            // filter twice
            $processedResult = $processor->filter($this->getPreparedTemplateText());
            $processedResult = $processor->filter($processedResult);
        }
        catch (Exception $e)   {
            $this->_cancelDesignConfig();
            throw $e;
        }
        $this->_cancelDesignConfig();
        return $processedResult;
    }
}