<?php

/**
 * Zeraga_Newsletter
 *
 * @category    Zeraga
 * @package     Zeraga_Newsletter
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Zeraga_Newsletter_Model_System_Config_Source_Scheduling_Date
{
    const MONDAY = 1;
    const TUESDAY = 2;
    const WEDNESDAY = 3;
    const THURSDAY = 4;
    const FRIDAY = 5;
    const SATURDAY = 6;
    const SUNDAY = 0;

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => self::MONDAY, 'label' => Mage::helper('zeraga_newsletter')->__('Monday')),
            array('value' => self::TUESDAY, 'label' => Mage::helper('zeraga_newsletter')->__('Tuesday')),
            array('value' => self::WEDNESDAY, 'label' => Mage::helper('zeraga_newsletter')->__('Wednesday')),
            array('value' => self::THURSDAY, 'label' => Mage::helper('zeraga_newsletter')->__('Thursday')),
            array('value' => self::FRIDAY, 'label' => Mage::helper('zeraga_newsletter')->__('Friday')),
            array('value' => self::SATURDAY, 'label' => Mage::helper('zeraga_newsletter')->__('Saturday')),
            array('value' => self::SUNDAY, 'label' => Mage::helper('zeraga_newsletter')->__('Sunday')),
        );
    }
}