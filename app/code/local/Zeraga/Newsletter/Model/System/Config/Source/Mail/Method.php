<?php

/**
 * Zeraga_Newsletter
 *
 * @category    Zeraga
 * @package     Zeraga_Newsletter
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Zeraga_Newsletter_Model_System_Config_Source_Mail_Method
{
    const MAIL = 1;
    const SMTP = 2;

    /**
     * Mailing method
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => self::MAIL, 'label' => Mage::helper('zeraga_newsletter')->__('Mail')),
            array('value' => self::SMTP, 'label' => Mage::helper('zeraga_newsletter')->__('SMTP'))
        );
    }
}