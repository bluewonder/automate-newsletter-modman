<?php
/**
 * Zeraga_Newsletter
 *
 * @category    Zeraga
 * @package     Zeraga_Newsletter
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Zeraga_Newsletter_Model_System_Config_Backend_Bestseller_Content extends Mage_Core_Model_Config_Data
{
    /**
     * Change this const value will make the newsletter stop working properly
     */
    const BESTSELLER_CONTENT_BLOCK_ID = 'newsletter_bestseller_content';
    /**
     * Create static block programmatically and set it content
     *
     * @return Zeraga_Newsletter_Model_System_Config_Backend_General_Content
     */
    protected function _beforeSave()
    {
        $storeCode   = $this->getStoreCode();
        $websiteCode = $this->getWebsiteCode();
        if ($websiteCode == '' && $storeCode == '')
        {
            $collection = Mage::getModel('cms/block')
                ->getCollection()
                ->addFieldToFilter('identifier', self::BESTSELLER_CONTENT_BLOCK_ID);
            $block = $collection->getFirstItem();
            if ($block->getId()) {
                $block->setData('content', $this->getValue());
                $block->save();
            } else {
                $block  = Mage::getModel('cms/block')->load(null);
                $block->setIdentifier(self::BESTSELLER_CONTENT_BLOCK_ID);
                $block->setTitle('Newsletter Bestseller Content');
                $block->setData('content', $this->getValue());
                $block->save();
            }
        }

    }
}