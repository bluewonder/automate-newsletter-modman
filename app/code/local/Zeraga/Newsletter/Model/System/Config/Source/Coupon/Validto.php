<?php

/**
 * Zeraga_Newsletter
 *
 * @category    Zeraga
 * @package     Zeraga_Newsletter
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Zeraga_Newsletter_Model_System_Config_Source_Coupon_Validto
{
    const ONE_DAY = '1';
    const THERE_DAYS = '2';
    const ONE_WEEK = '3';
    const UNTIL_END_OF_MONTH = '4';

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => self::ONE_DAY, 'label' => Mage::helper('zeraga_newsletter')->__('One Day')),
            array('value' => self::THERE_DAYS, 'label' => Mage::helper('zeraga_newsletter')->__('Three Days')),
            array('value' => self::ONE_WEEK, 'label' => Mage::helper('zeraga_newsletter')->__('One Week')),
            array('value' => self::UNTIL_END_OF_MONTH, 'label' => Mage::helper('zeraga_newsletter')->__('Until end of month')),
        );
    }
}