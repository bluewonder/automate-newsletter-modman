<?php

/**
 * Zeraga_Newsletter
 *
 * @category    Zeraga
 * @package     Zeraga_Newsletter
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Zeraga_Newsletter_Model_System_Config_Source_Category
{
    /**
     * Get category list from catalog/category collection
     *
     * @return array
     */
    public function toOptionArray()
    {
        $result = array(
            array('value' => 0, 'label' => '')
        );
        $collection = Mage::getModel('catalog/category')->getCollection();

        foreach ($collection as $category) {
            $category = Mage::getModel('catalog/category')->load($category->getId());
            if ($category->getName()) {
                $result[] = array('value' => $category->getId(), 'label' => $category->getName());
            }
        }
        return $result;
    }
}