<?php

/**
 * Zeraga_Newsletter
 *
 * @category    Zeraga
 * @package     Zeraga_Newsletter
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Zeraga_Newsletter_Model_System_Config_Source_Mail_Security
{
    const SSL = 'ssl';
    const TLS = 'tls';
    const NONE = 'none';

    /**
     * Put description here
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => self::SSL, 'label' => Mage::helper('zeraga_newsletter')->__('SSL')),
            array('value' => self::TLS, 'label' => Mage::helper('zeraga_newsletter')->__('TLS')),
            array('value' => self::NONE, 'label' => Mage::helper('zeraga_newsletter')->__('NONE')),
        );
    }
}