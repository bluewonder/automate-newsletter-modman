<?php
/**
 * Zeraga_Newsletter
 *
 * @category    Zeraga
 * @package     Zeraga_Newsletter
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Zeraga_Newsletter_Model_System_Config_Source_Newproduct_Amount
{
    /**
     * Options for amount selection
     *
     * @return array
     */

    public function toOptionArray()
    {
        return array(
            array('value' => 3, 'label' => 3),
            array('value' => 6, 'label' => 6),
            array('value' => 9, 'label' => 9),
        );
    }
}