<?php
/**
 * Zeraga_Newsletter
 *
 * @category    Zeraga
 * @package     Zeraga_Newsletter
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Zeraga_Newsletter_Model_System_Config_Source_Coupon_Coupon
{
    /**
     * Get coupon list from salesrule/coupon collection
     *
     * @return array
     */
    public function toOptionArray()
    {
        $result =  array(
            array('value' => 0,'label' => '')
        );
        $collection = Mage::getModel('salesrule/coupon')->getCollection();

        foreach($collection as $coupon) {
            $result[] = array('value' => $coupon->getId(),'label' => $coupon->getCode());
        }
        return $result;
    }
}