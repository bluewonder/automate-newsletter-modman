<?php

/**
 * Zeraga_Newsletter
 *
 * @category    Zeraga
 * @package     Zeraga_Newsletter
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Zeraga_Newsletter_Model_System_Config_Backend_Scheduling_Time extends Mage_Core_Model_Config_Data
{
    /**
     * Reformat the saved value
     *
     * @return Zeraga_Newsletter_Model_System_Config_Backend_Scheduling_Time
     */
    protected function _beforeSave()
    {
        if (is_array($value = $this->getValue())) {
            $hours = !empty($value[0]) ? $value[0] : 00;
            $mins = !empty($value[1]) ? $value[1] : 00;
            $ampm = !empty($value[2]) ? $value[2] : 1;

            if ($ampm == 2) {
                $hours += 12;
            }

            $this->setValue($hours . ',' . $mins . ',00');
        }
    }
}