<?php

/**
 * Zeraga_Newsletter
 *
 * @category    Zeraga
 * @package     Zeraga_Newsletter
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Zeraga_Newsletter_Model_System_Config_Source_Bestseller_Period
{
    const LAST30DAYS = '1';
    const LAST60DAYS = '2';
    const LASTYEAR = '3';
    const SAMEMONTHLASTYEAR = '4';
    const ALLTIME = '5';

    /**
     * Options for period selection
     *
     * @return array
     */
    public function toOptionArray()
    {
        $result = array(
            array('value' => 0, 'label' => ''),
            array('value' => self::LAST30DAYS, 'label' => Mage::helper('zeraga_newsletter')->__('Last 30 days')),
            array('value' => self::LAST60DAYS, 'label' => Mage::helper('zeraga_newsletter')->__('Last 60 days')),
            array('value' => self::LASTYEAR, 'label' => Mage::helper('zeraga_newsletter')->__('Last Year')),
            array('value' => self::SAMEMONTHLASTYEAR, 'label' => Mage::helper('zeraga_newsletter')->__('Same month previous year')),
            array('value' => self::ALLTIME, 'label' => Mage::helper('zeraga_newsletter')->__('All time')),
        );

        return $result;
    }
}