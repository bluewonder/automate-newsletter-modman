<?php

/**
 * Zeraga_Newsletter
 *
 * @category    Zeraga
 * @package     Zeraga_Newsletter
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Zeraga_Newsletter_Model_System_Config_Source_Scheduling_Frequency
{
    const WEEKLY = 1;
    const MONTHLY = 2;

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => self::WEEKLY, 'label' => Mage::helper('zeraga_newsletter')->__('Weekly')),
            array('value' => self::MONTHLY, 'label' => Mage::helper('zeraga_newsletter')->__('Monthly')),
        );
    }
}