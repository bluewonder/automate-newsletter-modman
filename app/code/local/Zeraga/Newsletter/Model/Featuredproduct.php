<?php
/**
 * Zeraga_Newsletter
 *
 * @category    Zeraga
 * @package     Zeraga_Newsletter
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Zeraga_Newsletter_Model_Featuredproduct extends Mage_Core_Model_Abstract
{
    const FEATURED_PRODUCT_ENABLE = 'zeraga_newsletter/featuredproduct/enable';
    const FEATURED_PRODUCT_HEADING = 'zeraga_newsletter/featuredproduct/heading';
    const FEATURED_PRODUCT_CAT = 'zeraga_newsletter/featuredproduct/products'; // Category ID
    const FEATURED_PRODUCT_AMOUNT = 'zeraga_newsletter/featuredproduct/amount';

    protected $_storeId;


    public function setStoreId($storeid)
    {
        $this->_storeId = $storeid;
        return $this;
    }

    public function getStoreId()
    {
        return $this->_storeId;
    }

    /**
     * Get if featured product display is enabled or not
     *
     * @return boolean
    */
    public function isEnable()
    {
        $storeId = $this->getStoreId();
        return Mage::getStoreConfig(self::FEATURED_PRODUCT_ENABLE, $storeId);
    }

    /**
     * Get all product in specific category in specific store id
     *
     * @return Mage_Catalog_Model_Resource_Product_Collection|boolean
    */
    public function getFeaturedProductCollection()
    {
        $storeId = $this->getStoreId();
        $productIds = $this->_getProductIds();
        $productCollection = Mage::getModel('catalog/product')->getCollection();
        $productCollection->addAttributeToSelect('*');
        $productCollection->addAttributeToFilter('status', array('eq' => 1));
        $productCollection->addAttributeToFilter('visibility', array('neq' => 1));
        if (!empty($productIds)) {
            $productCollection->addAttributeToFilter('entity_id', array('in', $productIds));
        }
        // limit product
        $amount = Mage::getStoreConfig(self::FEATURED_PRODUCT_AMOUNT, $storeId);
        $productCollection->getSelect()->limit($amount, 0);
        return $productCollection;
    }

    /**
     * Get all product ids in preset categories
     * @return array
     */
    protected function _getProductIds()
    {
        $storeId = $this->getStoreId();
        $categoryList = Mage::getStoreConfig(self::FEATURED_PRODUCT_CAT, $storeId);
        if ($found = strpos($categoryList, ',')  !== false) {
            $categoryList = explode(',',$categoryList);
        } else {
            $categoryList = array($categoryList);
        }

        $categoryResult = array();
        foreach($categoryList as $catId) {
            $category = Mage::getModel('catalog/category')->load($catId);
            $categoryResult = array_merge($categoryResult, $category->getAllChildren(true));
        }
        $categoryResult = array_unique($categoryResult);

        /**@var $zendDb Zend_Db_Adapter_Abstract */
        $zendDb = Mage::helper('zeraga_newsletter/sql')->getZendDB();
        $resource = Mage::getSingleton('core/resource');

        $select = $zendDb->select()
            ->from(array('cat' => $resource->getTableName('catalog/category_product')))
            ->where('category_id IN (?)', $categoryResult);
        $stmt = $zendDb->query($select);
        $productIds = array_unique($stmt->fetchAll(PDO::FETCH_COLUMN, 1));

        return $productIds;
    }
}