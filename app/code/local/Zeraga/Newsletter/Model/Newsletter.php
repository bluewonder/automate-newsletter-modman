<?php

/**
 * Zeraga_Newsletter
 *
 * @category    Zeraga
 * @package     Zeraga_Newsletter
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Zeraga_Newsletter_Model_Newsletter extends Mage_Core_Model_Abstract
{
    const MAIL_SENDER_EMAIL = 'zeraga_newsletter/general/emailfrom';
    const MAIL_SENDER_NAME = 'zeraga_newsletter/general/emailname';
    const MAIL_GENERAL_SUBJECT = 'zeraga_newsletter/general/subjectline';
    const MAIL_GENERAL_CONTENT = 'zeraga_newsletter/general/content';

    const TYPE_HTML = 2;

    /**
     * Email template
     */
    const NEWSLETTER_TEMPLATE_CONFIG_PATH = 'zeraga_newsletter/general/email_template';

    /**
     * Sending newsletter to customer in $customerCollection
     * @param $customerCollection Mage_Customer_Model_Resource_Customer_Collection
     */
    public function send($customerCollection)
    {
        /** @var $emailTemplate Zeraga_Newsletter_Model_Core_Email_Template  */
        $emailTemplate = Mage::getModel('zeraga_newsletter/core_email_template');
        if ($customerCollection->getSize() > 0) {
            Mage::getSingleton('zeraga_newsletter/coupon')->prepareCoupon();
        }
        foreach ($customerCollection as $receive) {
            Mage::unregister('current_customer');
            Mage::register('current_customer',$receive);
            $storeId = $receive->getStoreId();
            // Check if sending newsletter is enabled or not
            if (Mage::helper('zeraga_newsletter')->isEnable($storeId)) {
                $templateId = Mage::getStoreConfig(self::NEWSLETTER_TEMPLATE_CONFIG_PATH, $storeId);
                $vars = $this->_prepareVariable($storeId);

                // Add customer information
                $customerData = array_merge(array('name' => $receive->getName()), $receive->getData());
                $vars = array_merge($vars, array('customer' => new Varien_Object($customerData)));

                $sender = array(
                    'name' => Mage::getStoreConfig(self::MAIL_SENDER_NAME, $storeId),
                    'email' => Mage::getStoreConfig(self::MAIL_SENDER_EMAIL, $storeId)
                );

                $emailTemplate->sendTransactional($templateId, $sender, $receive->getEmail(), $receive->getName(), $vars, $receive->getStoreId());
                if ($emailTemplate->getSentSuccess()) {
                    $this->_markAsSentSuccess($receive->getId());
                }
            }
        }
    }

    /**
     * Send test email
    */
    public function sendTestEmail($config = array(), $transport)
    {
        /** @var $emailTemplate Zeraga_Newsletter_Model_Core_Email_Template  */
        $emailTemplate = Mage::getModel('zeraga_newsletter/core_email_template');

        $storeId = 0; // admin store
        $templateId = Mage::getStoreConfig(self::NEWSLETTER_TEMPLATE_CONFIG_PATH, $storeId);
        $vars = $this->_prepareVariable($storeId);

        // Add customer information
        if (strpos($config['name'], " ") !== false) {
            $customerName = explode(" ", $config['name']);
            $customerData = array(
                'name' => $config['name'],
                'firstname' => $customerName[0],
                'lastname' => $customerName[1],
                'email' => $config['email']
            );
        } else {
            $customerData = array(
                'name' => $config['name'],
                'email' => $config['email']
            );
        }


        $vars = array_merge($vars, array('customer' => new Varien_Object($customerData)));

        $sender = array(
            'name' => Mage::getStoreConfig(self::MAIL_SENDER_NAME, $storeId),
            'email' => Mage::getStoreConfig(self::MAIL_SENDER_EMAIL, $storeId)
        );

        $emailTemplate->sendTransactional($templateId, $sender, $config['email'], $config['name'], $vars, $storeId, $transport);
        return $emailTemplate->getSentSuccess();
    }

    /**
     * Prepare variables which use in email template
     * @param $storeId int: Customer store id
     * @return array
     */
    protected function _prepareVariable($storeId)
    {
        $general = array(
            'subject' => Mage::getStoreConfig(self::MAIL_GENERAL_SUBJECT, $storeId),
            'content' => Mage::getStoreConfig(self::MAIL_GENERAL_CONTENT, $storeId),
        );
        $couponId =  Mage::getStoreConfig(Zeraga_Newsletter_Model_Coupon::COUPON_SELECTED, $storeId);
        $coupon = Mage::getModel('salesrule/coupon')->load($couponId);
        $coupon = array(
            'heading' => Mage::getStoreConfig(Zeraga_Newsletter_Model_Coupon::COUPON_EMAIL_HEADING, $storeId),
            'coupon' => $coupon->getCode(),
        );

        return array(
            'general' => new Varien_Object($general),
            'coupon' => new Varien_Object($coupon),
        );
    }

    /**
     * After sending newsletter successfully,
     * Mark customer as sent newsletter
     * @param $customerId int
     */
    protected function _markAsSentSuccess($customerId)
    {
        $resource = Mage::getSingleton('core/resource');
        /** @var $zendDb Zend_Db_Adapter_Abstract */
        $zendDb = Mage::helper('zeraga_newsletter/sql')->getZendDB();
        $data = array(
            'entity_id' => $customerId
        );
        $update = array(
            'sent_newsletter' => 1
        );
        $zendDb->update($resource->getTableName('customer/entity'), $update, $data);
    }

    /**
     * Initialize coupon for customer right after newsletter is sent
     * @param $storeId int
     */

}