<?php

/**
 * Zeraga_Newsletter
 *
 * @category    Zeraga
 * @package     Zeraga_Newsletter
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Zeraga_Newsletter_Model_Coupon extends Mage_Core_Model_Abstract
{
    const COUPON_ENABLE = 'zeraga_newsletter/coupon/enable';
    const COUPON_SELECTED = 'zeraga_newsletter/coupon/coupon';
    const COUPON_VALIDTO = 'zeraga_newsletter/coupon/validto';
    const COUPON_EMAIL_HEADING = 'zeraga_newsletter/coupon/heading';

    protected $_storeId;


    public function setStoreId($storeid)
    {
        $this->_storeId = $storeid;
        return $this;
    }

    public function getStoreId()
    {
        return $this->_storeId;
    }

    /**
     * Get if featured product display is enabled or not
     *
     * @return boolean
     */
    public function isEnable()
    {
        $storeId = $this->getStoreId();

        return Mage::getStoreConfig(self::COUPON_ENABLE, $storeId);
    }


    public function addRecord($ruleId, $websiteId)
    {
        $helper = Mage::helper('zeraga_newsletter/sql');
        $resource = Mage::getSingleton('core/resource');
        $zendDB = $helper->getZendDB();
        $data = array(
            'rule_id' => $ruleId,
            'website_id' => $websiteId
        );
        $zendDB->insert($resource->getTableName('newsletter_coupon'), $data);
    }

    public function emptyTable()
    {
        $helper = Mage::helper('zeraga_newsletter/sql');
        $resource = Mage::getSingleton('core/resource');
        $zendDB = $helper->getZendDB();

        $zendDB->delete($resource->getTableName('newsletter_coupon'));
    }

    public function getAllRule()
    {
        return $this->_getColumnData(0);
    }

    public function getWebsitesByRule($ruleId)
    {
        $helper = Mage::helper('zeraga_newsletter/sql');
        $resource = Mage::getSingleton('core/resource');
        $zendDB = $helper->getZendDB();
        $select = $zendDB->select('website_id')
            ->from($resource->getTableName('newsletter_coupon'))
            ->where('rule_id = ?', $ruleId);
        $stmt = $zendDB->query($select);
        $result = $stmt->fetchAll(PDO::FETCH_COLUMN, 1);
        return array_unique($result);
    }

    public function setValidToByWebsiteId($websiteId, $validTo)
    {
        $helper = Mage::helper('zeraga_newsletter/sql');
        $resource = Mage::getSingleton('core/resource');
        /** @var $zendDB Zend_Db_Adapter_Abstract*/
        $zendDB = $helper->getZendDB();
        $data = array(
            'validto' => $validTo
        );
        $zendDB->update($resource->getTableName('newsletter_coupon'), $data, 'website_id = '.$websiteId.'');
    }


    public function getCouponData()
    {
        $helper = Mage::helper('zeraga_newsletter/sql');
        $resource = Mage::getSingleton('core/resource');
        $zendDB = $helper->getZendDB();
        $select = $zendDB->select()
            ->from($resource->getTableName('newsletter_coupon'));
        $stmt = $zendDB->query($select);
        $result = $stmt->fetchAll();
        return $result;
    }
    /**
     * Return value of validto by $ruleid and $websiteid
     * @rule_id int
     * @website_id int
    */
    public function getValidTo($ruleId, $websiteId)
    {
        $helper = Mage::helper('zeraga_newsletter/sql');
        $resource = Mage::getSingleton('core/resource');
        $zendDB = $helper->getZendDB();
        $select = $zendDB->select()
            ->from($resource->getTableName('newsletter_coupon'))
            ->where('rule_id = ?', $ruleId)
            ->where('website_id = ?', $websiteId);
        $stmt = $zendDB->query($select);
        $result = $stmt->fetchAll(PDO::FETCH_COLUMN, 2);
        return $result;
    }

    protected function _getColumnData($column)
    {
        $helper = Mage::helper('zeraga_newsletter/sql');
        $resource = Mage::getSingleton('core/resource');
        $zendDB = $helper->getZendDB();
        $select = $zendDB->select()
            ->from($resource->getTableName('newsletter_coupon'));
        $stmt = $zendDB->query($select);
        $result = $stmt->fetchAll(PDO::FETCH_COLUMN, $column);
        return array_unique($result);
    }

    /**
     * Insert valid time for each coupon data
     */

    public function prepareCoupon()
    {
        /** @var $model Zeraga_Newsletter_Model_Coupon */
        $model = Mage::getSingleton('zeraga_newsletter/coupon');
        $websiteCollection = Mage::getModel('core/website')->getCollection();
        foreach($websiteCollection as $website) {
            if ($website->getConfig(self::COUPON_ENABLE) == 1 && $website->getConfig(self::COUPON_SELECTED)) {
                $couponId = $website->getConfig(self::COUPON_SELECTED);
                $coupon = Mage::getModel('salesrule/coupon')->load($couponId);
                $ruleId = $coupon->getRuleId();

                $valid = $model->getValidTo($ruleId, $website->getId());
                if(($key = array_search(null, $valid)) !== false) {
                    unset($valid[$key]);
                }
                if (empty($valid)) {
                    /** @var  $dateHelper Zeraga_Newsletter_Helper_Day */
                    $dateHelper = Mage::helper('zeraga_newsletter/day');

                    $valid = $website->getConfig(Zeraga_Newsletter_Model_Coupon::COUPON_VALIDTO);
                    switch($valid) {
                        case Zeraga_Newsletter_Model_System_Config_Source_Coupon_Validto::ONE_DAY:
                            $model->setValidToByWebsiteId($website->getId(), $dateHelper->getTomorrow());
                            break;
                        case Zeraga_Newsletter_Model_System_Config_Source_Coupon_Validto::THERE_DAYS:
                            $model->setValidToByWebsiteId($website->getId(), $dateHelper->getDay(3));
                            break;
                        case Zeraga_Newsletter_Model_System_Config_Source_Coupon_Validto::ONE_WEEK:
                            $model->setValidToByWebsiteId($website->getId(), $dateHelper->getDay(7));
                            break;
                        case Zeraga_Newsletter_Model_System_Config_Source_Coupon_Validto::UNTIL_END_OF_MONTH:
                            $model->setValidToByWebsiteId($website->getId(), $dateHelper->getFirstDayOfNextMonth());
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }
}