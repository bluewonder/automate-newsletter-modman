<?php

/**
 * Zeraga_Newsletter
 *
 * @category    Zeraga
 * @package     Zeraga_Newsletter
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Zeraga_Newsletter_Model_Observer
{
    const CRON_EXPRESSION_PATH = 'crontab/jobs/zeraga_newsletter/schedule/cron_expr';
    const CRON_EXPRESSION_CONFIGURATION_PATH = 'zeraga_newsletter/scheduling/cronexpression';
    const TIME_CONFIGURATION_PATH = 'zeraga_newsletter/scheduling/time';
    const FREQUENCY_CONFIGURATION_PATH = 'zeraga_newsletter/scheduling/frequency';
    const DATE_CONFIGURATION_PATH = 'zeraga_newsletter/scheduling/date';
    const MONTHLY_CONFIGURATION_PATH = 'zeraga_newsletter/scheduling/monthly';

    const COUPON_VALIDTO_PATH = 'zeraga_newsletter/coupon/validto';
    const COUPON_ENABLE_PATH = 'zeraga_newsletter/coupon/enable';

    /**
     * Generate Cron Expression
     */
    public function adminSystemConfigChangedSectionBwNewsletter($observer)
    {
        // Rebuild the table newsletter_coupon
        $this->_initializeCoupon();
    }

    /**
     * get cron expression as in array type
     * @return array
     */
    protected function _getCronExpression()
    {
        $time = Mage::getStoreConfig(self::TIME_CONFIGURATION_PATH);
        $frequency = Mage::getStoreConfig(self::FREQUENCY_CONFIGURATION_PATH);
        // Default Cron Expression
        $result = array(
            0 => '*',
            1 => '*',
            2 => '*',
            3 => '*',
            4 => '*',
        );

        // translate hours and min to cron expression
        $time = explode(',', $time);
        $result[0] = !empty($time[1]) ? $time[1] : 0; // Min
        $result[1] = !empty($time[0]) ? $time[0] : 0; // Hours

        // check if schedule is weekly or monthly
        if ($frequency == Zeraga_Newsletter_Model_System_Config_Source_Scheduling_Frequency::WEEKLY) {
            $date = Mage::getStoreConfig(self::DATE_CONFIGURATION_PATH);
            $result[4] = $date;
        } else if ($frequency == Zeraga_Newsletter_Model_System_Config_Source_Scheduling_Frequency::MONTHLY) {

            $month = Mage::getStoreConfig(self::MONTHLY_CONFIGURATION_PATH);
            $monthArr = explode(',', $month);
            $rank = $monthArr[0];
            $day = $monthArr[1];

            $monthRate = $monthArr[2];

            switch ($rank) {
                case 1 :
                    $result[2] = '1-7';
                    break;
                case 2 :
                    $result[2] = '8-14';
                    break;
                case 3 :
                    $result[2] = '15-21';
                    break;
                case 4 :
                    $result[2] = '22-28';
                    break;
                default:
                    break;
            }

            $result[3] = '1/' . $monthRate;
            $result[4] = $day;

        }
        return $result;
    }

    public function doSomething($schedule)
    {
        // read config and fill data to newsletter_coupon
        $websiteCollection = Mage::getModel('core/website')->getCollection();
        $helper = Mage::helper('zeraga_newsletter/day');
        foreach($websiteCollection as $website) {
            if ($website->getConfig(self::COUPON_ENABLE_PATH)) {
                $validToConfig = $website->getConfig(self::COUPON_VALIDTO_PATH);
                $validTo = '';
                switch ($validToConfig) {
                    case 1:
                        $validTo = $helper->getToday();
                        break;
                    case 2:
                        $validTo = $helper->getDay(3);
                        break;
                    case 3:
                        $validTo = $helper->getDay(7);
                        break;
                    case 4:
                        $validTo = $helper->getTheLastOfMonth();
                        break;
                    default:
                        $validTo = '0000-00-00';
                        break;
                }
                Mage::getModel('zeraga_newsletter/coupon')->setValidToByWebsiteId($website->getId(),$validTo);
            }
        }
    }

    /**
     * Sending newsletter email to customer.
     * This only send newsletter to specific amount of customers to avoid timeout
     */
    public function scheduledSend($schedule)
    {
        $countOfCustomer = 20;
        $resource = Mage::getSingleton('core/resource');
        $collection = Mage::getModel('customer/customer')->getCollection()
            ->setPageSize($countOfCustomer)
            ->setCurPage(1);
        $collection->getSelect()->where('sent_newsletter = 0');
        $collection
            ->getSelect()
            ->join(array('nw' => $resource->getTableName('newsletter/subscriber')), 'e.entity_id = nw.customer_id AND nw.subscriber_status = 1');
        Mage::getModel('zeraga_newsletter/newsletter')->send($collection);

        // Generate scheduler for reseting newsletter sending
        $scheduleAheadFor = Mage::getStoreConfig(Mage_Cron_Model_Observer::XML_PATH_SCHEDULE_AHEAD_FOR)*60;
        $schedule = Mage::getModel('cron/schedule');
        $jobCode = 'zeraga_newsletter';
        $result = $this->_getCronExpression();
        $cronExpr = implode(' ', $result);

        $now = time();
        $schedules = $schedule->getCollection();
        $schedules->addFieldToFilter('status', Mage_Cron_Model_Schedule::STATUS_PENDING)
        ->load();
        $exists = array();
        foreach ($schedules->getIterator() as $schedule) {
            $exists[$schedule->getJobCode().'/'.$schedule->getScheduledAt()] = 1;
        }
        $timeAhead = $now + $scheduleAheadFor;
        $schedule->setJobCode($jobCode)
            ->setCronExpr($cronExpr)
            ->setStatus(Mage_Cron_Model_Schedule::STATUS_PENDING);

        for ($time = $now; $time < $timeAhead; $time += 60) {
            $ts = strftime('%Y-%m-%d %H:%M:00', $time);
            if (!empty($exists[$jobCode.'/'.$ts])) {
                // already scheduled
                continue;
            }
            if (!$schedule->trySchedule($time)) {
                // time does not match cron expression
                continue;
            }
            $schedule->unsScheduleId()->save();
        }

    }

    /**
     * Reset the newsletter sending
     */

    public function resetNewsletter()
    {
        $resource = Mage::getSingleton('core/resource');
        $writeAdapter = $resource->getConnection('core_write');
        $zendDb = Zend_Db::factory('Pdo_Mysql', $writeAdapter->getConfig());
        $data = array(
            'sent_newsletter' => 1
        );
        $zendDb->update($resource->getTableName('customer/entity'), $data);
    }
    /**
     * Add coupon to websites base on configuration
    */
    protected function _initializeCoupon() {
        $couponModel = Mage::getModel('zeraga_newsletter/coupon');
        // Remove all website of old coupon (set in table newsletter_coupon)
        $allRule = $couponModel->getAllRule();
        foreach($allRule as $rule) {
            $rule = Mage::getModel('salesrule/rule')->load($rule);
            if ($rule->getId()) {
                $websiteIds = $couponModel->getWebsitesByRule($rule->getId());
                if ($rule->getWebsiteIds() == array() || $this->_popArray($rule->getWebsiteIds(),$websiteIds) == array()) {
                    $rule->setWebsiteIds(array(0));
                } else {
                    $rule->setWebsiteIds($this->_popArray($rule->getWebsiteIds(),$websiteIds));
                }
                $rule->save();
            }
        }

        // Empty the table newsletter_coupon
        $couponModel->emptyTable();

        // Rebuild data for table newsletter_coupon
        $websiteCollection = Mage::getModel('core/website')->getCollection();
        $couponPath = Zeraga_Newsletter_Model_Coupon::COUPON_SELECTED;
        $couponEnablePath = Zeraga_Newsletter_Model_Coupon::COUPON_ENABLE;
        foreach($websiteCollection as $website) {
            $couponId = $website->getConfig($couponPath);
            $couponEnable = $website->getConfig($couponEnablePath);
            if ($couponEnable == 1) {
                $coupon = Mage::getModel('salesrule/coupon')->load($couponId);
                $ruleId = $coupon->getRuleId();
                if ($ruleId) {
                    $couponModel->addRecord($ruleId,$website->getId());
                }
            }
        }
        // Get data from newsletter_coupon and applied website data

        $allRule = $couponModel->getAllRule();

        foreach($allRule as $rule) {
            $rule = Mage::getModel('salesrule/rule')->load($rule);
            $websiteIds = $couponModel->getWebsitesByRule($rule->getId());
            $result = array_merge($rule->getWebsiteIds(),$websiteIds);
            if (!empty($result)) {
                $rule->setWebsiteIds(array_unique($result));
            } else {
                $rule->setWebsiteIds(0);
            }
            $rule->save();
        }
    }

    protected function _popArray($arr,$arrPop) {
        foreach($arrPop as $e) {
            if (($key = array_search($e, $arr)) !== false ) {
                unset($arr[$key]);
            }
        }
        return $arr;
    }
    /**
     * each day, this function is call to check if the coupon still valid for each website
    */
    public function couponValidtoCalculating()
    {
        $newsletterCouponData = Mage::getModel('zeraga_newsletter/coupon')->getCouponData();
        foreach($newsletterCouponData as $coupon) {
            $validto = $coupon['validto'];
            if ($validto <= Mage::helper('zeraga_newsletter/day')->getYesterday()) {
                // remove that website from current coupon
                $salesRule = Mage::getModel('salesrule/rule')->load($coupon['rule_id']);
                $websiteIds = $salesRule->getWebsiteIds();
                if(($key = array_search($coupon['website_id'], $websiteIds)) !== false) {
                    unset($websiteIds[$key]);
                    $salesRule->setWebsiteIds(array_merge($websiteIds,array(0)));
                    $salesRule->save();
                }
            }
        }
    }

    /**
     * Hide some auto-generate static block from static block grid
    */

    public function coreCollectionAbstractLoadBefore($observer)
    {
        $collection = $observer->getCollection();
        if (Mage::app()->getRequest()->getControllerName() != 'cms_block') return;
        // Hide some block which auto-generate
        if (get_class($collection) == 'Mage_Cms_Model_Resource_Block_Collection' || get_parent_class($collection) == 'Mage_Cms_Model_Resource_Block_Collection') {
            $notShowBlock = array(
                Zeraga_Newsletter_Model_System_Config_Backend_Bestseller_Content::BESTSELLER_CONTENT_BLOCK_ID,
                Zeraga_Newsletter_Model_System_Config_Backend_Coupon_Content::COUPON_CONTENT_BLOCK_ID,
                Zeraga_Newsletter_Model_System_Config_Backend_Featuredproduct_Content::FEATUREDPRODUCT_CONTENT_BLOCK_ID,
                Zeraga_Newsletter_Model_System_Config_Backend_General_Content::GENERAL_CONTENT_BLOCK_ID,
                Zeraga_Newsletter_Model_System_Config_Backend_Newproduct_Content::NEWPRODUCT_CONTENT_BLOCK_ID
            );
            $collection->addFieldToFilter('identifier', array('nin' => $notShowBlock));
        }
    }


}