<?php
/**
 * Zeraga_Newsletter
 *
 * @category    Zeraga
 * @package     Zeraga_Newsletter
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Zeraga_Newsletter_Helper_Day
{
    public function getTheLastOfMonth()
    {
        $today = date("Y-m-d", Mage::getModel('core/date')->timestamp(time()));
        $tomorrow = date("Y-m-d", Mage::getModel('core/date')->timestamp(time() + (1 * 24 * 60 * 60)));
        if (substr($tomorrow,-2,2) != '01') {
            $time = time() + (1 * 24 * 60 * 60);
            while(substr($tomorrow,-2,2) != '01') {
                $today = date("Y-m-d", Mage::getModel('core/date')->timestamp($time));
                $time += (1 * 24 * 60 * 60);
                $tomorrow = date("Y-m-d", Mage::getModel('core/date')->timestamp($time));
            }
        }
        return $today;
    }

    public function getFirstDayOfNextMonth()
    {
        $today = date("Y-m-d", Mage::getModel('core/date')->timestamp(time()));
        $tomorrow = date("Y-m-d", Mage::getModel('core/date')->timestamp(time() + (1 * 24 * 60 * 60)));
        if (substr($tomorrow,-2,2) != '01') {
            $time = time() + (1 * 24 * 60 * 60);
            while(substr($tomorrow,-2,2) != '01') {
                $today = date("Y-m-d", Mage::getModel('core/date')->timestamp($time));
                $time += (1 * 24 * 60 * 60);
                $tomorrow = date("Y-m-d", Mage::getModel('core/date')->timestamp($time));
            }
        }
        return $tomorrow;
    }

    public function getYesterday()
    {
        return date("Y-m-d", Mage::getModel('core/date')->timestamp(time() - (1 * 24 * 60 * 60)));
    }
    public function getToday()
    {
        return date("Y-m-d", Mage::getModel('core/date')->timestamp(time()));
    }

    public function getTomorrow()
    {
        return date("Y-m-d", Mage::getModel('core/date')->timestamp(time() + (1 * 24 * 60 * 60)));
    }

    public function getDay($day)
    {
        return date("Y-m-d", Mage::getModel('core/date')->timestamp(time() + ($day * 24 * 60 * 60)));
    }
}