<?php

/**
 * Zeraga_Newsletter
 *
 * @category    Zeraga
 * @package     Zeraga_Newsletter
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Zeraga_Newsletter_Helper_Data extends Mage_Core_Helper_Abstract
{
    const ENABLE_CONFIG_PATH = 'zeraga_newsletter/general/enable';
    /**
     * Check if module enable
    */
    public function isEnable($store = null)
    {
        return Mage::getStoreConfig(self::ENABLE_CONFIG_PATH,$store);
    }
}