<?php

/**
 * Zeraga_Newsletter
 *
 * @category    Zeraga
 * @package     Zeraga_Newsletter
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Zeraga_Newsletter_Helper_Sql
{
    /**
     * Check the existence of column in specific table
     * @param $tableName string
     * @param $columnName string
     * @return boolean
     */
    public function columnExist($tableName, $columnName)
    {
        $resource = Mage::getSingleton('core/resource');
        $writeAdapter = $resource->getConnection('core_write');

        Zend_Db_Table::setDefaultAdapter($writeAdapter);
        $table = new Zend_Db_Table($tableName);
        if (!in_array($columnName, $table->info('cols'))) {
            return false;
        }
        return true;
    }

    /**
     * Get Zend_Db for directly update db
     */

    public function getZendDB()
    {
        $resource = Mage::getSingleton('core/resource');
        $writeAdapter = $resource->getConnection('core_write');
        $zendDb = Zend_Db::factory('Pdo_Mysql', $writeAdapter->getConfig());

        return $zendDb;
    }
}