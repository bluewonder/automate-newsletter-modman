<?php

/**
 * Zeraga_Newsletter
 *
 * @category    Zeraga
 * @package     Zeraga_Newsletter
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Zeraga_Newsletter_Helper_Mail
{
    const MAIL_HOST = 'zeraga_newsletter/mail/stmp_server_address';
    const MAIL_SECURITY = 'zeraga_newsletter/mail/security';
    const MAIL_PORT = 'zeraga_newsletter/mail/port';
    const MAIL_USERNAME = 'zeraga_newsletter/mail/username';
    const MAIL_PASSWORD = 'zeraga_newsletter/mail/password';
    const EMAIL_METHOD = 'zeraga_newsletter/mail/method';

    /**
     * Get config to know if newsletter is sent using smtp config
     * @return boolean
     */
    public function sendMailUsingSmtp()
    {
        return Mage::getStoreConfig(self::EMAIL_METHOD) == Zeraga_Newsletter_Model_System_Config_Source_Mail_Method::SMTP;
    }

    /**
     * @return Zend_Mail_Transport_Smtp
     */
    public function getTransport($id = null)
    {
        $username = Mage::getStoreConfig(self::MAIL_USERNAME, $id);
        $password = Mage::getStoreConfig(self::MAIL_PASSWORD, $id);
        $host = Mage::getStoreConfig(self::MAIL_HOST, $id);
        $port = Mage::getStoreConfig(self::MAIL_PORT, $id);
        $ssl = Mage::getStoreConfig(self::MAIL_SECURITY, $id);

        // Set up the config array

        $config = array();
        $config['auth'] = 'login';
        $config['username'] = $username;
        $config['password'] = $password;


        if ($port) {
            $config['port'] = $port;
        } else {
            $config['port'] = 25;
        }

        if ($ssl != "none") {
            $config['ssl'] = $ssl;
        }

        $transport = new Zend_Mail_Transport_Smtp($host, $config);
        return $transport;
    }
}