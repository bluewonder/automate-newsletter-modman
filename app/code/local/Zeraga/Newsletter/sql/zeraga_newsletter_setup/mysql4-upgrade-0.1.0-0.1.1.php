<?php
/**
 * Zeraga_Newsletter
 *
 * @category    Zeraga
 * @package     Zeraga_Newsletter
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

// General block content
$block  = Mage::getModel('cms/block')->load(null);
$block->setIdentifier(Zeraga_Newsletter_Model_System_Config_Backend_General_Content::GENERAL_CONTENT_BLOCK_ID);
$block->setTitle('Newsletter General Content');
$block->setData('content', "Hi  {{var customer.name}},<br /> This month we have some great specials, a new exclusive coupon for you, and a quick look at some of our bestsellers and new specials.");
$block->save();

// Coupon block content
$block  = Mage::getModel('cms/block')->load(null);
$block->setIdentifier(Zeraga_Newsletter_Model_System_Config_Backend_Coupon_Content::COUPON_CONTENT_BLOCK_ID);
$block->setTitle('Newsletter Coupon Content');
$block->setData('content', 'Enter the coupon code at checkout to receive your discount!<br />  Coupon code: {{var coupon.coupon}} <- Enter this code at checkout!');
$block->save();

// Bestseller block content
$block  = Mage::getModel('cms/block')->load(null);
$block->setIdentifier(Zeraga_Newsletter_Model_System_Config_Backend_Bestseller_Content::BESTSELLER_CONTENT_BLOCK_ID);
$block->setTitle('Newsletter Bestseller Content');
$block->setData('content', 'Browse our popular products');
$block->save();

// Featured block content
$block  = Mage::getModel('cms/block')->load(null);
$block->setIdentifier(Zeraga_Newsletter_Model_System_Config_Backend_Featuredproduct_Content::FEATUREDPRODUCT_CONTENT_BLOCK_ID);
$block->setTitle('Newsletter Featured Product Content');
$block->setData('content', 'Take a look at this month’s featured products');
$block->save();

// New product block content
$block  = Mage::getModel('cms/block')->load(null);
$block->setIdentifier(Zeraga_Newsletter_Model_System_Config_Backend_Newproduct_Content::NEWPRODUCT_CONTENT_BLOCK_ID);
$block->setTitle('Newsletter New Product Content');
$block->setData('content', 'Browse the latest arrival to our store');
$block->save();