<?php
/**
 * Zeraga_Newsletter
 *
 * @category    Zeraga
 * @package     Zeraga_Newsletter
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();
$helper = Mage::helper('zeraga_newsletter/sql');
if (!$helper->columnExist($this->getTable('customer/entity'),'sent_newsletter')) {
    $installer->run("
    ALTER TABLE `{$this->getTable('customer/entity')}`
    ADD `sent_newsletter` smallint(1) DEFAULT 0;
    ");
}

$installer->run("
DROP TABLE IF EXISTS {$this->getTable('newsletter_coupon')};
CREATE TABLE {$this->getTable('newsletter_coupon')} (
  `rule_id` varchar(255) NOT NULL,
  `website_id`  varchar(20) NOT NULL,
  `validto` DATE NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");


$installer->endSetup();