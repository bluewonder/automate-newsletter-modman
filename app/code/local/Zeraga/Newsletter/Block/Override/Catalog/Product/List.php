<?php
/**
 * Wishlattedesk_Customerrelationship
 *
 * @category    Wishlattedesk
 * @package     Wishlattedesk_Customerrelationship
 * @copyright   Copyright (c) 2014 Wishlattedesk Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author      Hieu Nguyen (Wishlattedesk's team)
 * @email       bzaikia@gmail.com
 */  
class Zeraga_Newsletter_Block_Override_Catalog_Product_List extends Mage_Catalog_Block_Product_List {

    /**
     * Price template
     *
     * @var string
     */
    protected $_priceBlockDefaultTemplate = 'zeraga/catalog/product/price.phtml';
}