<?php
/**
 * Zeraga_Newsletter
 *
 * @category    Zeraga
 * @package     Zeraga_Newsletter
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Zeraga_Newsletter_Block_Catalog_Product_List extends Mage_Catalog_Block_Product_List {
    /**
     * Price template
     *
     * @var string
     */
    protected $_priceBlockDefaultTemplate = 'zeraga/catalog/product/price.phtml';
}