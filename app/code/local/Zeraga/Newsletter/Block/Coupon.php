<?php

/**
 * Zeraga_Newsletter
 *
 * @category    Zeraga
 * @package     Zeraga_Newsletter
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Zeraga_Newsletter_Block_Coupon extends Zeraga_Newsletter_Block_Abstract
{
    const COUPON_HEADING = 'zeraga_newsletter/coupon/heading';

    /** @var $_model Zeraga_Newsletter_Model_Coupon */
    protected $_model;

    public function _construct()
    {
        $this->_customer = Mage::registry('current_customer');
        $this->_model = Mage::getModel('zeraga_newsletter/coupon');
    }

    /**
     * return boolean
     */
    public function canShow()
    {
        $this->_model->setStoreId($this->getStoreId());
        return $this->_model->isEnable();
    }

    public function getHeading()
    {
        return Mage::getStoreConfig(self::COUPON_HEADING, $this->getStoreId());
    }

    public function getCoupon()
    {
        $couponId =  Mage::getStoreConfig(Zeraga_Newsletter_Model_Coupon::COUPON_SELECTED, $this->getStoreId());
        $coupon = Mage::getModel('salesrule/coupon')->load($couponId);
        return $coupon->getCode();
    }
}