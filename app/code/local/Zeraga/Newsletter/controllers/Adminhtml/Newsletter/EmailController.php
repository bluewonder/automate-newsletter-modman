<?php

/**
 * Zeraga_Newsletter
 *
 * @category    Zeraga
 * @package     Zeraga_Newsletter
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Zeraga_Newsletter_Adminhtml_Newsletter_EmailController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Send test email
     */

    public function sendAction()
    {
        $params = $this->getRequest()->getParams();

        $transport = false;

        $result = array(
            'error' => false,
            'message' => ''
        );

        if (empty($params['name']) || empty($params['email'])) {
            $result['error'] = true;
            $result['message'] = $this->__('Please provide test name and test email');
            $this->getResponse()->setBody(json_encode($result));
            return;
        }
        Mage::register('test_email', $params['email']);
        if (!empty($params['email_method']) && $params['email_method'] == 2) {
            if (empty($params['host']) || empty($params['ssl']) || empty($params['port']) || empty($params['username']) || empty($params['password'])) {
                $result['error'] = true;
                $result['message'] = $this->__('Please fill all require value in email configuration');
                $this->getResponse()->setBody(json_encode($result));
                return;
            }
            $transport = $this->_getTransport($params);

        }

        $receiver = array(
            'name' => $params['name'],
            'email' => $params['email']
        );

        $sent = Mage::getModel('zeraga_newsletter/newsletter')->sendTestEmail($receiver, $transport);
        if (!$sent) {
            $result['error'] = true;
            $result['message'] = $this->__('Email configuration is incorrect');
            $this->getResponse()->setBody(json_encode($result));
        }

        $this->getResponse()->setBody(json_encode($result));
        return;
    }

    protected function _getTransport($params)
    {
        $username = $params['username'];
        $password = $params['password'];
        $host = $params['host'];
        $port = $params['port'];
        $ssl = $params['ssl'];

        // Set up the config array

        $config = array();
        $config['auth'] = 'login';
        $config['username'] = $username;
        $config['password'] = $password;


        if ($port) {
            $config['port'] = $port;
        }

        if ($ssl != "none") {
            $config['ssl'] = $ssl;
        }

        $transport = new Zend_Mail_Transport_Smtp($host, $config);
        return $transport;
    }
}