<?php

/**
 * Zeraga_Newsletter
 *
 * @category    Zeraga
 * @package     Zeraga_Newsletter
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Varien_Data_Form_Element_Monthly extends Varien_Data_Form_Element_Abstract
{
    public function __construct($attributes = array())
    {
        parent::__construct($attributes);
        $this->setType('timehalf');
    }

    public function getName()
    {
        $name = parent::getName();
        if (strpos($name, '[]') === false) {
            $name .= '[]';
        }
        return $name;
    }

    public function getElementHtml()
    {
        $this->addClass('select');
        $rank = array(
            1 => 'First',
            2 => 'Second',
            3 => 'Third',
            4 => 'Fourth',
        );

        $dates = array(
            2 => 'Monday',
            3 => 'Tuesday',
            4 => 'Wednesday',
            5 => 'Thursday',
            6 => 'Friday',
            7 => 'Saturday',
            8 => 'Sunday',
        );

        $value = $this->getValue() ? explode(',', $this->getValue()) : array();
        $defaultValues = array(
            0 => 1,
            1 => 2,
            2 => 1,
        );

        if (is_array($value)) {
            $defaultValues[0] = !empty($value[0]) ? $value[0] : $defaultValues[0];
            $defaultValues[1] = !empty($value[1]) ? $value[1] : $defaultValues[1];
            $defaultValues[2] = !empty($value[2]) ? $value[2] : $defaultValues[2];
        }

        $html = '<input type="hidden" id="' . $this->getHtmlId() . '" />';
        $html .= 'The ';
        $html .= '<select name="' . $this->getName() . '" ' . $this->serialize($this->getHtmlAttributes()) . ' style="width:120px">' . "\n";
        for ($i = 1; $i <= 4; $i++) {
            $html .= '<option value="' . $i . '" ' . (($defaultValues[0] == $i) ? 'selected="selected"' : '') . '>' . $rank[$i] . '</option>';
        }
        $html .= '</select>' . "\n";

        $html .= '<select name="' . $this->getName() . '" ' . $this->serialize($this->getHtmlAttributes()) . ' style="width:150px">' . "\n";
        for ($i = 2; $i < 8; $i++) {
            $html .= '<option value="' . $i . '" ' . (($defaultValues[1] == $i) ? 'selected="selected"' : '') . '>' . $dates[$i] . '</option>';
        }
        $html .= '</select>' . "\n";
        $html .= "\n";
        $html .= 'of every ';

        $html .= '<select name="' . $this->getName() . '" ' . $this->serialize($this->getHtmlAttributes()) . ' style="width:40px">' . "\n";
        for ($i = 1; $i <= 12; $i++) {
            $html .= '<option value="' . $i . '" ' . (($defaultValues[2] == $i) ? 'selected="selected"' : '') . '>' . $i . '</option>';
        }
        $html .= '</select>' . "\n";
        $html .= ' Month';
        $html .= $this->getAfterElementHtml();
        return $html;


    }
}